# Cyber Crime Through the Eyes of a Normal Internet User #

Pros and cons of the cyber space:

As always the good and the evil are like two sides of a coin. Though the advantages outweigh the disadvantages it is high time we looked into them before they spread. Computer, internet or information technology related crimes are termed as cyber crimes. So what really is cyber crime? How seriously can these crimes affect common man or business establishments? What actions can be taken to curb such crimes and what legal action is needed to restrict the criminals who are into this? Cyber crime is growing to be a greatest threat to mankind and hence serious considerations and actions are definitely needed to check its spreading further.

We need to first understand what cyber crime is and what all activities are termed as cyber crimes. Cyber crime simply means criminal activities using the telecommunication and internet technology. Basically cyber crimes can be categorized into two types. They are,

Crimes which are directly targeted on vast computer networks or other related devices. For example, malware, computer viruses, attacks on denial of services etc
Crimes that are facilitated by the computer networks targeted not on any independent network or device. For example, identity theft, cyber stalking, phishing, email scams, hacking etc.
Cyber crimes are also considered or at least in lieu with white collar crimes, as the culprit cannot be conned very easily as the internet is open to the wide world. Although most cyber crimes are not related to any violence or serious crime still identity threat or government secrets can be virtually stolen in no time if the security measures fail. Terrorist, deviants and extremists stand higher chances of using this technology for their illegal and anti social activities.

Internet related frauds:

Almost 163 million users are estimated to use the internet this year as against just 16.1 million in 1995. thus the need for a more secured network becomes imperative keeping in mind the user's overall security.

The anonymous nature of the internet is the breeding ground for criminals who engage in activities which are termed as virtual crimes or cyber crimes. Governments have introduced cyber laws to curb such activities but then no serious punishment is awarded to the criminal or individuals who are involved in cyber crimes. Proper legal infrastructure is yet to be created and powerful regulatory mechanisms formed to protect netizens.

Let us now have a look of the different types of cyber law and the amount of damage they release to the society or individual. Generally the cyber threats sources are of three types. They are,

hacking related threats
traditional criminal threats
ideology Threats
Hacking:

This is one of the common forms of cyber crime found in the cyber space worldwide. It has been defined as "whoever with the intent to cause or knowing that he is likely to cause wrongful loss or damage to the public or any person destroys or deletes or alters any information residing in a computer resource or diminishes its value or utility or affects it injuriously by any means commits hacking". A hacker can crack into computer system thus gaining access to all internal information. Many hackers just do it for fun or pastime. Hacking is considered less harmful than other security related crimes.

Basically the hacker's main aim is to disrupt a system or network. Whether he is a white hat hacker or black hat hacker his level of destruction is to stop or get the access to the computer systems. Repeated hacking or tampering constantly might take a hacker behind bars but many times these crimes are taken lightly.

Traditional cyber crimes

Criminals whose focus is on monetary gains only are called traditional cyber criminals. Most of them are identified as some internal source. Recent study has confirmed that almost 80% criminals of such crimes belong to the related company or firm. Industrial espionage, intellectual property crime, trademark violation, illegal fund transfers, credit card scams, etc are some of the traditional cyber crimes. Such criminals who conduct these crimes are more likely to end up behind bars if the crime is proved.

Ideology cyber threats:

Stolen data are circulated as against the intellectual property laws according to such ideology threats. These criminals consider themselves as Robin Hood and spread the data which is preserved under intellectual property rights. Many terrorist activities are also termed as ideology threats in the cyber world. They spread their own ideology or oppose government's by using the internet technology. Cyberanarchistsis how they are called and their primary aim is to spread their ideology or principles and opposing what is against their activities. Many terrorists' plans and data's are also considered as cyber threats.

Thus whatever be the nature of cyber crime strict laws must be administered to enable a secured cyber space. As more and more of our daily activities becomes connected or interlinked in cyber space the need for a complete secure technology has become the need of the hour. Be is simple email hacking or phishing, the people involved in such activities are definitely invading the privacy of individuals and business organizations. Identity thefts, money swindling and credit card scams are grave issues which can cause irreparable damage to the person concerned.

Prevention is definitely better than cure:

How can we prevent our network or computer systems against the so the cyber criminals? How can the government aid in curbing such high risk threats to the society? As individuals it is important that we use the best internet security system to protect our systems from cyber attacks. It is important to use strong password to protect the emails or other important data or document stored online. Important details like bank account user names and passwords must not be stored online or in the computer. Remember that the cyber space is an open network and has no security or safety against such important data.

Never open unidentified emails and never reply or believe in email scams saying that you have won millions of dollars in an online lottery. Credit cards must be used sparingly or wisely online. Unsecured sites and restricted sites are always high on risk and thus using your credit cards on such sites is highly unadvisable. Always keep changing passwords and install a powerful anti virus software to protect against torzons, viruses and malware.

Companies and business establishment must ask the workers to sign powerful contracts to prevent internal identity thefts. The servers and domains must be well secured for continuous cyber security for their data. Government datas and highly secretive data must be under strict scrutiny. Hackers are also used by enemy nations to fid out the internal secrets of an opponent country. So, all forms of top secret is better not stored online. Too much information exposed is always a cyber threat. Vigilance, awareness and not sharing personal information while networking can prevent most of such cyber crimes.

The government must setup cyber wings to identify such criminals and put forth strong rules or punishment for cyber criminals. Cyber laws must be very strict and newer technology must be used to easily find these criminals to curb their illegal activity. Steps to block the erring person and information updated on such criminals can help the general public from identifying the different crimes in the cyber space. After all knowledge is what can make one powerful, isn't it?

* [https://www.onsist.com/local-government-protection/](https://www.onsist.com/local-government-protection/)
